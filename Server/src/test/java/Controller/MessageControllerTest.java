package Controller;

import Models.Message;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MessageControllerTest {

    @Test
    void getMessage() {
        // Arrange
        MessageController controller = new MessageController();

        // Act
        Response result = controller.GetMessage("Test");

        // Assert
        System.out.println(result);
    }
}