package DataContext;

import Models.Message;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.sql.*;
import java.util.ListIterator;

public class MessageDataContext {
    public List<Message> GetMessagesByChatId(String chatId){
        List<Message> messages = null;

        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con=DriverManager.getConnection("jdbc:mysql://192.168.178.51:3306/Test","monitor","password");
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select * from Test");
            while(rs.next()) {
                messages.add(new Message(Integer.toString(rs.getInt(2)),"",LocalDateTime.now()));
            }
            con.close();
        }catch(Exception e){
            System.out.println(e);
        }
        return messages;
    }
}
