package Controller;

import DataContext.MessageDataContext;
import Models.Message;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/message")
public class MessageController {


    @GET
    @Path("/GetMessages/{ChatId}")
    public Response GetMessage(@PathParam("ChatId") String chatId){
        MessageDataContext messageDataContext = null;
        List<Message> messages;
        messages = messageDataContext.GetMessagesByChatId(chatId);
        return Response.ok(messages).build();
    }
}
