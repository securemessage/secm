package Models;

import lombok.*;

import java.time.LocalDateTime;

public class Message {
    @Getter @Setter private String ChatId;
    @Getter @Setter private String Text;
    @Getter @Setter private LocalDateTime CreationDate;
    public Message(String _ChatId, String _Text, LocalDateTime _CreationDate){
        ChatId = _ChatId;
        Text = _Text;
        CreationDate = _CreationDate;
    }
}
